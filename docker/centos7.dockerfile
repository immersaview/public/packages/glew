# Centos docker image located at https://hub.docker.com/_/centos/
FROM centos:7

RUN yum makecache \
&&  yum install -y \
    scl-utils \
    centos-release-scl \
    libXmu-devel \
    libXi-devel \
    libGL-devel

RUN  yum install -y devtoolset-7 \
&&  scl enable devtoolset-7 bash

RUN yum install -y epel-release

RUN yum install -y cmake3 \
&&  ln -s /usr/bin/cmake3 /usr/bin/cmake \
&&  ln -s /usr/bin/cpack3 /usr/bin/cpack

CMD ["bash"]
