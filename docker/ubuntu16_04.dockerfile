# Ubuntu docker image located at https://hub.docker.com/_/ubuntu/
FROM ubuntu:16.04

RUN apt-get update \
 && apt-get install -y --no-install-recommends \
    build-essential \
    libxmu-dev \
    libxi-dev \
    libgl-dev \
    cmake

CMD ["bash"]
