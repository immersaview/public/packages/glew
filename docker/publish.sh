#!/usr/bin/env bash
set  -e

USAGE="Usage: portal_build <all|filename> <rebuild | force build and publish the image, even if it already exists>"

if [ $# -lt 1 ]; then
    echo "Insufficient arguments"
    echo ${USAGE}
    exit 1
fi

if [[ "${2}exists" == "exists" ]]; then
    REBUILD="false"
elif [[ "${2}" == "rebuild" ]]; then
    REBUILD="true"
else
    echo "Invalid argument '${2}'"
    echo ${USAGE}
fi

VERSION="1.0.0"
IMAGE_NAME="glew-build"

does_docker_image_exists() {
    RESULT=`curl -s -H "X-Jfrog-Art-Api:${DOCKER_SERVER_PASSWORD}" -X GET "http://${DOCKER_SERVER_URL}/artifactory/api/docker/docker-local/v2/${IMAGE_NAME}/tags/list" | grep "${1}" --quiet`
    echo $?
}

build_docker_image() {
    FILEPATH="${1}"
    FILENAME="`basename "${FILEPATH}" .dockerfile`"

    TAG="${VERSION}-${FILENAME}"
    
    if [[ `does_docker_image_exists "${TAG}"` -ne 0 || "${REBUILD}" == "true" ]]; then
        echo "Building Docker Image ${TAG} ..."
        docker build "./docker/" -f "${FILEPATH}" -t "${TAG}"

        IMAGE_ID="`docker images -q "${TAG}"`"
        LOCATION="${DOCKER_SERVER_URL}/${IMAGE_NAME}:${TAG}"

        docker login -u "${DOCKER_SERVER_USERNAME}" -p "${DOCKER_SERVER_PASSWORD}" "${DOCKER_SERVER_URL}"
        docker tag "${IMAGE_ID}" "${LOCATION}"
        docker push "${LOCATION}"
        docker logout "${DOCKER_SERVER_URL}"
    else
        echo "Docker image ${IMAGE_NAME}:${TAG} already built. Skipping."
    fi
}

ARG="${1}"
if [[ "${ARG}" == "all" ]]; then
    for DOCKERFILE in "./docker/"*.dockerfile; do
        build_docker_image "${DOCKERFILE}"
    done
else
    build_docker_image "${ARG}"
fi
