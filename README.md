## Example CMake Usage:
```cmake
target_link_libraries(target glew)
target_include_directories(target PRIVATE "$<TARGET_PROPERTY:glew,INTERFACE_INCLUDE_DIRECTORIES>")
```